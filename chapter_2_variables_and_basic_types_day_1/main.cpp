//
// Created by yuyang li on 21/1/21.
//

#include <iostream>


using namespace std;

int main() {
    bool b = 42;
    int i = b;
    i = 3.14;
    double pi = i;
    unsigned char c = -1;
    unsigned uc = c;
    signed char c2 = 256;
    cout << b << endl;
    cout << i << endl;
    cout << pi << endl;

    // c holds value of 255, due to its unsigned char type, its value has been wrapped around
    // since 255 represent nothing on ASCii table, so the output is a ? mark
    cout << "c: " << c << endl;
    cout << "c: " << (int) c << endl;
    cout << "uc: " << uc << endl;
    cout << "c2: " << c2 << endl;
    cout << "c2: " << (int) c2 << endl;

    unsigned u = 10;
    int v = -42;
    cout << v + v << endl;
    cout << u + v << endl;

    cout << "exercises Section 2.1.2 page 67\n";
    unsigned uInt1 = 10, uInt2 = 42;
    cout << uInt1 - uInt2 << endl;
    cout << uInt2 - uInt1 << endl;

    int int1 = 10, int2 = 42;
    cout << int1 - int2 << endl;
    cout << int2 - int1 << endl;

    cout << int1 - uInt1 << endl;
    cout << uInt1 - int1 << endl;

    // Generalized escape
    // Octal digits version
    // \145 -> e check ASCii for detail
    cout<< "Who goes with F\145rgus?\012";

    cout << "reference section test code: \n";
    int iVal = 12;
    int &refVal = iVal;
    cout << &iVal << endl;
    cout << refVal << endl;
    cout << &refVal << endl;
    cout << endl;
    iVal = 13;
    cout << &iVal << endl;
    cout << refVal << endl;
    cout << &refVal << endl;
    cout << endl;

    int &refVal2 = refVal;
    refVal = 14;
    cout << &refVal2 << ": " << refVal2 <<endl;
    cout << iVal << endl;
    cout << endl;

    int i216 = 0, &ri216 = i;
    double d216 = 0, &rd216 = d216;
    rd216 = 3.14159;
    rd216 = ri216;
    cout << typeid(rd216).name() << endl;
    i216 = rd216;
    ri216 = rd216;
    cout << ri216 << endl;
    cout << rd216 << endl;

    return 0;
}