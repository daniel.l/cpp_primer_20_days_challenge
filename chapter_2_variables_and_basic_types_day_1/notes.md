# Chapter 2. Variables and Basic Types -- Notes

## 2.1 Primitive Built-in Types
### 2.1.1 Arithmetic Types
Two categories:

 - integral types
 - floating-point types

To give meaning to memory at a given address, we must know the type of the value stored there.
The type determines how many bits are used and how to interpret those bits.


### 2.1.2 Type Conversion
If we assign an out-of-range value to an object of unsigned type, 
the result is the remainder of the value modulo the number of values the target type can hold.

If we assign an out-of-range value to an object of signed type, 
the result is 
> undefined

The program might appear to work, it might crash, or it might produce garbage values.

### 2.1.3 Literal
Character and Character String Literals

| Prefix | Meaning | Type |
| ------ | ------- | ---- |
| u | Unicode 16 character | char16_t |
| U | Unicode 32 character | char32_t |
| L | wide character | wchar_t |
| u8 | utf-8 (string literals only) | char |

Integer Literals

| Suffix | Minimum Type |
| ------ | ------------ |
| u or U | unsigned | 
| l or L | long | 
| ll or LL | long long | 

Floating-Point Literals

| Suffix | Minimum Type |
| ------ | ------------ |
| f or F | float | 
| l or L | long double | 

Generalized escape:

`\x` followed by one or more hexadecimal digits

`\` followed by one, two or three octal digits
 
## 2.2 Variables
### 2.2.1 Variable Definitions

Variable Definitions

| type specifier | list of variable names | semicolon |
| -------------- | ---------------------- | --------- |
|       int      | sum = 0, value, units  |     ;     |

Initializer

Initialization and assignment are different operations in C++

- Initialization happens when a variable is given a value when it is created. 
- Assignment obliterates an object's current value and replaces that value with a new one.

Variations of initializer

- these two are normal ones

    `int units_sold = 0` 

    `int units_sold(0)`

- List Initializer (introduced in standard 11)

    `int units_sold = {0}` 
  
    `int units_sold{0}`
  
One important property of list initializer:
    
- The compiler will not let programmers list initialize variables of build-in
type if the initializer might lead to the loss of information.
  
    `long double id = 3.1415926536`
  
    `int a{id}, b{id};` // error: narrowing conversion required
  
    `int c(id), d(id);` // ok: but value will be truncated
  
Default Initialization

- Variables defined outside any function body are initialized to zero. 
  (with one exception see detail in section 6.1.1 page 270)
- Uninitialized objects of built-in type defined inside a function body 
  have undefined value.
- Objects of class type that we do not explicitly initialize have a value 
  that is defined by the class.
  
### 2.2.2 Variable Declarations and Definitions

- Declaration: a declaration makes a name known to the program. 
  it specifies the type and name of a variable.
- Definition: a variable definition is a declaration. 
  In addition to specifying the name and type, a definition also allocates
  storage and may provide the variable with an initial value.
  
To obtain a declaration that is not also a definition, 
we add the `extern` keyword and may not provide an explicit initializer.

`extern int i;` // declares but does not define i

`int j;` // declares and defines

An `extern` that has an initializer is a definition, 
this doing overrides the `extern`

`extern double pi = 3.1416`

### 2.2.3 Identifiers

Skipped

### 2.2.4 Scope of Name

Nested Scopes

- `::` scope operator

- Global Scope has no name.

## 2.3 Compound Types
### 2.3.1 References

Reference: a reference defines an alternative name for an object. A reference itself is not a object,
it's just another name for an already existing object.
  
  `int ival = 1024;`

  `int &refVal = ival;` // refVal refers to ival

  `int &refVal2;` // error: a reference must be initialized

- When a variable has been initialized, the value of the initializer is copied
  into the object that created
- When defining a reference, instead of copying the initializer's value,
  c++ binds the reference to its initializer. Once initialized, a reference remains bound to its initial object.
  - There is no way to rebind a reference to refer to a different object.
  - Hence, a reference must be initialized, due to the restriction to rebind a reference
  
Reference Definitions

- A reference must be bound only to an object, not literal or to the result of a more general expression.
- The type of a reference and the object to which the reference refers must match exactly.

### 2.3.2 Pointers

Pointer: 
  - Like reference, pointers are used for indirect access to other objects.
  - Unlike reference, a pointer is an object in its own right.
    - Pointers can be assigned and copied
    - A single pointer can point to several different objects over its lifetime.
    - A pointer doesn't need to be initialized at the time it is defined.
    - Like other built-in types, pointers defined at block scope have undefined value if they are not initialized.

Taking the Address of an Object

`&` address-of operator

`int iVal = 42;`

`int *p = &iVal;`

- The type of pointer must match the type of object it points to.

Pointer Value

The value stored in a pointer can be in one of four stats:
1. It can point to an object.
2. It can point ot the location just immediately past the end of an object.
3. It can be a null pointer, indicating that it is not bound to any object.
4. It can be invalid; values other that the preceding three are invalid.

Even cases 2 and 3 are valid, there are limits on what we can do with such pointers. 
Because these pointers do not point to any object,we may not use them to access the object to which the pointer points. 
The behavior of access an object through such pointers is undefined.

Using a Pointer to Access an Object

`*` dereference operator

`int iVal = 42;`

`int *p = &iVal;`

`cout << *p << endl;`

We may dereference only a valid pointer that points to an object.

Null Pointers

A null pointer does not point to any object

Ways to obtain a null pointer:
- `int *p1 = nullptr;`
- `int *p2 = 0;`
- `int *p3 = NULL` // must #include cstdlib

`nullptr` was introduced in new standard which is a literal that has a special type that can be converted to any other pointer type.

`NULL` is a preprocessor variable defined in cstdlib header with value 0, which mean the second and third method are doing the same behavior.

Normally, pointer is an object (unlike reference) that used to access other object indirectly (like reference) , as mentioned above.
The reason that pointer can be initialized with 0 literal is, I believe, c++ has made some sort of interning optimization for 0.
Literal 0 may hold its own static value for efficiency purpose.

void* Pointers

The type void* is a special pointer type that can hold the address of any object.
It holds an address, but the type of the object at that address is unknown.

void* type pointers can be used only:
- compare it to another pointer
- pass it ot or return it from a function
- assign it to another void* pointer

We cannot use a void* type pointer to operate on the object it addresses, 
cause we don't know the type of that object it points to, 
and the type determines what operations we can perform on the object.

Generally, we use a void* to deal with memory as memory, rather than access objects indirectly.

### 2.3.3 Understanding Compound Type Declarations

Base type: arithmetic types and void type, like int, float and void...
Type modifier: * and &

Reference to Pointers:

A reference is not an object. Hence, we may not have a pointer to reference. 
However, because a pointer is an object, we can define a reference to a pointer.

Note: 

A reference has to match the type, 

not just the base type which means the type modifier needs to be matched as well, 

it refers to, for instance

`int *p;`

`int *&r = p`

Declare a reference &r to the type of int *

### 2.4 const Qualifier

Once a variable type has been defined with key word `const`, 
any attempt to assign to the variable is an error.

Because the immutability of const variable, the const variable must be initialized.

By Default, `const` Objects Are Local to a File

When we define a `const` with the same name in multiple files, 
it is as if we had written definitions for separate variables in each files.

In order to share the const variables across multiple files but whose initializer is not a constant expression.

`const int bufSize = func();`

we need to use the `extern` key word on both its definition and declaration.

// file.cpp

`extern const int bufSize = fun();`

// file.h

`extern const int bufSize;` // here is the same bufSize which defined in file.cpp

### 2.4.1 References to const

we can use reference to const to bind a reference to a object with const type. Unlike ordinary reference, reference to const type could not be used to change the value of underling object it binds to.

which means the object with an explicit const type can only bind with

`const type(int,double, float...) &reference_name`

because the type of reference must match the type of the object it binds to

Initialization And References to const

Two exceptions to the rule of

 > the type of reference must match the type of the object it refers to

- We can initialize a reference to const to a object that can be converted to the reference type: which means we can bind a reference to const to a **non const object**, a **literal** or a **more general expression**
- We can bind a pointer or reference to a base-class type to an object of a type derived from that base class.

It is important to realized that a reference to const restricts only what we can do through that reference directly. Binding a reference to const to an object says nothing about whether the underlying object itself is const.

`int i = 42;`

`int &r1 = i; // r1 bound to i`

`const int &r2 = i; // r2 also bound to i; but cannot be used to change i`

`r1 = 0; // r1 is not const; i is now 0`

`r2 = 0; // error: r2 is a reference to const` 

### 2.4.2 Pointer and const

For the two exceptions of type matching rule, Pointer is same as Reference

Const Pointers

Again, unlike reference, pointers are objects. Hence, as with any other object type, the pointer itself can be a const.

Like any other const object, a const pointer must be initialized, and itsvalue remains immutable once initialized.

We indicate that the pointer is const by putting the const after the *. THis placement indicates that it is the pointer, not the pointed-to type, that is const.

`const double pi = 3.14159;`

`const double* const pip = &pi; // pip is a const pointer to a const object`

### 2.4.3 Top-Level const

top-level const: used to indicate whether the pointer itself is a const or not;

low-level const: used to indicate the pointed object is const type;

During the **copy operation**, the top-level const can be ignored; on the other hand, the low-level const can never be ignored.

### 2.4.4 constexpr and Constant Expressions

Constant Expressions

A constant expression is an expression whose value cannot be changed and that can be evaluated at compile time. For instace:

- a literal
- a const object that is initialized from a constant expression

`int staff_size = 27; // not a constant expression`

`const int sz = get_size(); // not a constant expression`

Although staff_size is initialized from a literal, it is not a constant expression because it is a plain int, not a const int.

Even though sz is a const, the value of its initializer is not known until run time. Hence, sz is not a constant expression.

constexpr Variables

> Under C++11 standard: we can ask the compiler to verify that a variable is a constant expression by declaring the variable in a constexpr declaration. Variablesdeclared as constexpr are implicitly const and must be initialized by constant expression.

`constexpr int mf = 20; // constant expression`

`constexpr int limit = mf + 1; // constant expression`

`constexpr int sz = size(); // sz is a constexpr expression only when size() retuens a constant expression`

> The ordinary function as an initializer for constexpr is forbidden, C++11 allows us to define certain functions as constexpr. Such functions must be simple enought that the compiler can evaluate them during the compile time.

Literal Types

The types we can use in a constexpr declaration are known as literal type, because they are simple enough to have literal values.

- Primitive types
- Literal classed (check 7.5.6 for details)
- Enumerations (check 19.3 for details)

Although we can define both Pointer and Reference as constexpr, the objects we use to initialize them are strictly limited.

We can initialize a constexpr pointer from:
- the nullptr literal 
- The literal 0
- The object with fixed address

> Variables defined inside functions ordinarily are note stored at a fixed address (related to the heap and stack maybe, check 6.1.1 for details)

Pointers and constexpr

The constexpr declaration for pointer imposed a top-level const on the objects it defines.

`const int *p = nullptr; // p is a pointer to a const int`
`constexpr int *q = nullptr; // q is a const pointer to int`

like any other const pointer, a constexpr pointer may point to a const or a non const type

## 2.5 Dealing with Types

Complications in using types arise in two different ways:

- Some types are hard to spell, which means, they have forms that are tedious and error-prone to write in code.
- some types are hard to determined during implementation. 

### 2.5.1 Type Aliases

Type aliases let programmers simplify complicated type definitions, making those types easier to use.

Type aliases also let us emphasize the purpose for which a type is used.

Traditional way:

`typedef double wages; // wages is a synonym for double`

`typedef wages base, *p; // base is a synonym for double, p for double *`

C++11 way:

`using SI = Sales_item; // SI is a synonym for Sales_item`

Pointers, const, and Type Aliases

Delarations that use type aliases that represent compond types and const can yield surprising results.

Keeping in mind of what the base type is holds the key to understand this issue, because const appears in the base type modifies the given type.

`typedef char *pstring;`

`const pstring cstr = 0; // cstr is a constant pointer to char`

`const pstring *ps; // ps is a pointer to a constant pointer to char`

The base type here is a pointer to char, due to the declaration to pstring, so the const affects on the base type, in this case, a pointer to char.

> It can be tempting, albeit incorrect, to interpret a declaration that uses a type alias by conceptually replacing the alias with its corresponding type:
>
> `const char *cstr = 0; // this represent a pointer to const char, because the base type here is char not char*`

### 2.5.2 The auto Types Specifier

It's sometime suprisingly difficult to determine the type of an expression. In such case, using the auto type specifier can do the trick.

> A variable that uses auto as tis type specifier must have an initializer:
> 
> `auto item = val1 + val2; // item initialized to the result of val1 + val2, the type of item is deduced from the type of the result of adding val1 + val2`

As with any other type specifier, we can define multiple variables using auto. Because a declaration can involve only a signle base type, the initializers for all the variables in the declaration must have types that are consistent with each other:

`auto i = 0, *p = &i; // i is int and p is a pointer to int, the base type here are both int`

`auto sz = 0, pi = 3.14; // error: insonsistent types of sz and pi`

Conpound Types, const, and auto

The type that the complier infers for auto is not always exactly the same as the initializer's type. instead, the compiler adjust the type to the conform to normal initialization rules.

For instance, when we use a reference, we are actually using the underlying object it refers to. In particular, when we use a reference as an initializer, the initializer is the corresponding object. The compiler uses that object's type for auto's type deduction:

`int i = 0, &r = i;`

`auto a = r; // r is a reference to int object, so the variable a here has type int not reference to int`

auto ordinarily ignores top-level const

