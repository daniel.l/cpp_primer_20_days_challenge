//
// Created by yuyang li on 24/2/21.
//

#include <iostream>
#include <string>

using namespace std;

int main() {
    // 3.2.2
//    string s;
//    cin >> s; // read a whitespace-separated string into s
//    cout << s << endl;

//    string word;
//    while (cin >> word)// how to simulate eof from user input on mac?
//        cout << word << endl;

    string line;
    while (getline(cin, line))
//        if (!line.empty())
        if (line.size() > 20)
            cout << line << endl;

    return 0;
}