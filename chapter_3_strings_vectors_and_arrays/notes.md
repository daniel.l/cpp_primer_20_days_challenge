# Chapter 3. Strings, Vectors, and Arrays

## 3.2 Library string Type

### 3.2.2 Operations on Strings

The string::size_type Type

size() function defined in string library returns a string::size_type value.

> The string class, and most other library types, defines several companion types. This companion types make it possible to use the library types in a machine independent manner. The type size_type is one of these companion types. To use the size_type defined by string, we use the scope operator to say that the name size_type is defined in the string class.

The precise type of string::size_type is uncertained, only to be sure is the string::size_type is an unsigned type and can hold the size of any string.

Any variable used to store the result from the string size operation should be of type string::size_type.

> It is essential to remember that expressions that mix signed and unsigned data can have surprising results. For example, if n is an int that holds a negative value, then s.size() < n will almost surely evaluate as true. It yields true because the negative value in n will convert to a large unsigned value.

    int n = -5;
    string sizeTest;
    sizeTest = "size test";
    cout << "start size test\n";
    if (sizeTest.size() < n) {
        cout << "what do you expect?" << endl;
    } else {
        cout << "really?" << endl;
    }

Adding Literals and Strings

When we mix strings and string or character literals, **at least one operand to each + operator** must be of string type:

 
    string s1 = "hello", s2 = "world";
    string s3 = s1 + ", " + s2 + '\n';
    string s4 = s1 + ", ";
    string s5 = "hello" + ", "; // error: no string operand
    string s6 = s1 + ", " + "world";
    string s7 = "hello" + ", " + s2; // error: can't add string literals
    string s8 = "hello" + s6 + " stupid\n"; // for each + operator, at least one operand must be of string type

string s6 could be considered as:

    string s6 = (s1 + ", ") + "world";

`(s1 + ", ")` will return a string type

### 3.2.3 Dealing with the Characters in a string

Processing Every Character with Range-Based for

    for(declaration : expression)
        statement

## 3.3 Library vector Type

### 3.3.1 
